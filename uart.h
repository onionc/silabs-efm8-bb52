/*
 * uart.h
 *
 *  Created on: 2022年1月13日
 *      Author: chadJ
 */

#ifndef UART_H_
#define UART_H_
#define RX_BUF_MAX_LEN 32
typedef struct
{
    uint8_t rx_buff[RX_BUF_MAX_LEN];
    uint8_t len;
} URAT_RX;

void uartInit();
void uartSend(char *buf);
void uartClearBuf();

void msgHandler(char *str, void(*func)(void), char all);
void funcLed0();
void funcLed1();

#endif /* UART_H_ */
