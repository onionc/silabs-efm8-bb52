/*
 * motor.h
 *
 *  Created on: 2022年1月16日
 *      Author: chadJ
 */

#ifndef MOTOR_H_
#define MOTOR_H_

void motorInit();

void motorStep(uint8_t step, uint8_t dir);

#endif /* MOTOR_H_ */
