/*
 * delay.c
 * 延时，使用计时器0中断完成精准的延时函数
 *  Created on: 2022年1月13日
 *      Author: chadJ
 */
#include "common.h"
#include "delay.h"

void Timer0Init(){
  /**
   * timer0使用分频后的时钟
   */
  CKCON0 |= CKCON0_T0M__PRESCALE;

  // 工作方式
  TMOD = TMOD | TMOD_T0M__MODE1 | TMOD_CT0__TIMER | TMOD_GATE0__DISABLED;




  // IE |= XXX; // 不使用中断，所以不用设置IE位

}

void Timer0_Delay1ms (uint16_t ms)
{

    uint16_t i;

    for (i = 0; i < ms; i++) {         // Count microseconds
        TCON &= ~0x30;                   // STOP Timer0 and clear overflow
        TH0 = (-SYSCLK/12/1000) >> 8;    // 1 ms
        TL0 = -SYSCLK/12/1000;

        TCON |= 0x10;                    // START Timer0
        while (TCON_TF0 == 0);           // Wait for overflow
    }

}
