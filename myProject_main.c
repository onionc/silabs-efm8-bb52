//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include "common.h"
#include "delay.h"

#include "uart.h"
#include "motor.h"
#include "ired.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------

extern URAT_RX uart0;

//-----------------------------------------------------------------------------
// SiLabs_Startup() Routine
// ----------------------------------------------------------------------------
// This function is called immediately after reset, before the initialization
// code is run in SILABS_STARTUP.A51 (which runs before main() ). This is a
// useful place to disable the watchdog timer, which is enable by default
// and may trigger before main() in some instances.
//-----------------------------------------------------------------------------
void SiLabs_Startup (void)
{
  // 关闭看门狗
  SFRPAGE = 0;
  WDTCN = 0xDE;
  WDTCN = 0xAD;
}


int main (void)
{
  uint8_t step = 0;
  uint8_t SFRPAGE_save = SFRPAGE;
  // 初始化
  init();

  // led 初始化
  ledInit();

  // 定时器0初始化 delay
  Timer0Init();

  // 串口初始化
  uartInit();

  // 电机初始化
  motorInit();

  // 红外初始化
  iredInit();
  SFRPAGE = SFRPAGE_save;
  IE_EA = 1; // 总中断

  P1_B4 = 1; // 点亮 LED0



  while (1) {

      /*
      // 测试串口
      Timer0_Delay1ms(500);
      uartSend(uart0.rx_buff);


      msgHandler("LED1", funcLed1, 1);
      msgHandler("LED0", funcLed0, 1);
      msgHandler("clear", uartClearBuf, 0);
      */


      // 测试电机
      Timer0_Delay1ms(1);
      motorStep(step++, 1);
      if(step>=8) step=0;



  }
}
