/*
 * delay.h
 *
 *  Created on: 2022年1月13日
 *      Author: chadJ
 */

#ifndef DELAY_H_
#define DELAY_H_

void Timer0Init();

void Timer0_Delay1ms (uint16_t);

#endif /* DELAY_H_ */
