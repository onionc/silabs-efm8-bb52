/*
 * common.h
 *
 *  Created on: 2022年1月13日
 *      Author: onion
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <SI_EFM8BB52_Register_Enums.h>

#define SYSCLK       24500000/8         // Clock speed in Hz
#define UART0_BAUD         115200

void init(); // 初始化函数
void ledInit();

void itoa( int i,char* string);

#endif /* COMMON_H_ */
