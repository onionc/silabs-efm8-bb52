/*
 * common.c
 *
 *  Created on: 2022年1月14日
 *      Author: chadJ
 */

#include "common.h"


void init(){
    //  启用 Crossbar
    XBR2 = XBR2|XBR2_XBARE__ENABLED;

    /**
     * 设置晶振为12分频
     */
    CKCON0 = CKCON0_SCA__SYSCLK_DIV_12;

    TMOD = 0;

    IE = 0;

    // set clk 24.5MHz/8  默认的
    // CLKSEL = CLKSEL_CLKSL__HFOSC0_clk24p5 | CLKSEL_CLKDIV__SYSCLK_DIV_8;

    // wait ready
    //while ((CLKSEL & CLKSEL_DIVRDY__BMASK) == CLKSEL_DIVRDY__NOT_READY);
}

void ledInit(){
    P1MDIN |=  P1MDIN_B4__DIGITAL; // 设置 P1.4 输入模式
    P1MDOUT |= P1MDOUT_B4__PUSH_PULL; // 设置 P1.4 输出模式
}


void itoa( int i,char* string)
{
    int power, j;
    j=i;
    for (power=1;j>=10;j/=10)
        power*=10;
    for (;power>0;power/=10)
    {
        *string++='0'+i/power; i%=power;
    }
    *string='\0';
}

