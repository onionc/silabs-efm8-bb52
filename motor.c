/*
 * motor.c
 *
 * 步进电机驱动，IO控制
 *  Created on: 2022年1月16日
 *      Author: chadJ
 */
#include "common.h"
#include "motor.h"



void motorInit(){
  P1MDIN = P1MDIN | P1MDIN_B0__DIGITAL | P1MDIN_B1__DIGITAL | P1MDIN_B2__DIGITAL | P1MDIN_B3__DIGITAL; // 设置 P1.0~3 输入模式
  P1MDOUT = P1MDOUT | P1MDOUT_B0__PUSH_PULL | P1MDOUT_B1__PUSH_PULL | P1MDOUT_B2__PUSH_PULL | P1MDOUT_B3__PUSH_PULL; // 设置 P1.0~3 输出模式
}


void motorStep(uint8_t step, uint8_t dir)
{
  uint8_t temp=step;
  if(dir==0) //如果为逆时针旋转
    temp=7-step;//调换节拍信号
  switch(temp)//8 个节拍控制：A->AB->B->BC->C->CD->D->DA
  {
    case 0: P1_B0=1;P1_B1=0;P1_B2=0;P1_B3=0;break;
    case 1: P1_B0=1;P1_B1=1;P1_B2=0;P1_B3=0;break;
    case 2: P1_B0=0;P1_B1=1;P1_B2=0;P1_B3=0;break;
    case 3: P1_B0=0;P1_B1=1;P1_B2=1;P1_B3=0;break;
    case 4: P1_B0=0;P1_B1=0;P1_B2=1;P1_B3=0;break;
    case 5: P1_B0=0;P1_B1=0;P1_B2=1;P1_B3=1;break;
    case 6: P1_B0=0;P1_B1=0;P1_B2=0;P1_B3=1;break;
    case 7: P1_B0=1;P1_B1=0;P1_B2=0;P1_B3=1;break;
  }
}

