/*
 * ired.c
 * 红外，使用外部中断
 *  Created on: 2022年1月16日
 *      Author: chadJ
 */

#include "common.h"
#include "ired.h"


void iredInit(){
  P0_B3=1;
  P0MDOUT |= P0MDOUT_B3__SHIFT;
      P0MDIN |= P0MDIN_B3__DIGITAL;


    // 控制位：int0 边沿触发
      TCON |= TCON_IT1__EDGE;
              // 低电位有效 | 设置P0.3为int0的中断IO
              IT01CF = IT01CF_IN1PL__ACTIVE_LOW | IT01CF_IN1SL__P0_3;




    IE |= IE_EX1__ENABLED;

}



SI_INTERRUPT(INT1_ISR, INT1_IRQn)
{

  P1_B4 = !P1_B4;

}


