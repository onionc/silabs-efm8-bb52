/*
 * uart.c
 *
 * 串口通信，使用了计时器1和串口中断
 *  Created on: 2022年1月13日
 *      Author: chadJ
 */
#include "common.h"
#include "uart.h"
#include <string.h>
//-----------------------------------------------------------------------------
// Global CONSTANTS
//-----------------------------------------------------------------------------
URAT_RX uart0;
URAT_RX rece0;
uint8_t tx_flag = 0;

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------


void Timer1Init(){
  CKCON0 |= CKCON0_T1M__SYSCLK;

  /**
   * Mode 2, 8-bit Counter/Timer with  Auto-Reload
   */
  TMOD = TMOD | TMOD_T1M__MODE2 | TMOD_CT1__TIMER | TMOD_GATE1__DISABLED;

  TCON &= ~TCON_TR1__BMASK;                   // STOP Timer1


  TH1 = 0xFF-(SYSCLK/UART0_BAUD)/2;

  TCON = TCON| TCON_TR1__RUN;                    // START Timer1

}


void uartInit(){
  // time1
  Timer1Init();

  // 9位失效 | 允许接收
  // SCON0_MCE__MULTI_DISABLED |
  SCON0 = SCON0 | SCON0_REN__RECEIVE_ENABLED;

  P0MDIN =  P0MDIN | P0MDIN_B4__DIGITAL | P0MDIN_B5__DIGITAL ; // 设置 P0.4 0.5 输入模式
  P0MDOUT = P0MDOUT_B4__PUSH_PULL | P0MDOUT_B5__OPEN_DRAIN; // 设置 P0.4 0.5 输出模式

  // P0.4 0.5 开启
  XBR0 = XBR0|XBR0_URT0E__ENABLED;

  IE = IE | IE_ES0__ENABLED;

}


void uartSend(char *buf)
{
  uint8_t count = 0;
  SCON0_TI = 0;
  while (buf[count] != '\0')
  {
    tx_flag = 0;
    SBUF0 = buf[count];
    while (!tx_flag)
      ;
    count++;
  }

  for(count=0; count<uart0.len; count++)
    {
      tx_flag = 0;
      SBUF0 = uart0.rx_buff[count];
      while (!tx_flag)
        ;
    }
}

void uartClearBuf(){
  uart0.len=0;
  uart0.rx_buff[0]='\0';
}


/**
 * 消息处理函数
 * @param str
 * @param func
 * @param all 类型，1则全等，0则模糊查询
 */
void msgHandler(char *str, void(*func)(void), char all){

  bool flag = false;
  if(all==1){
      flag = strcmp(uart0.rx_buff, str)==0;
  }else{
      flag = strstr(uart0.rx_buff, str)!=NULL;
  }
  if(flag){
      func();
      uartClearBuf();
  }
}


void funcLed0(){
  P1_B4 = 0;
}

void funcLed1(){
  P1_B4 = 1;
}


/*******************************************************************************
Function:       // UART0_IRQn
Description:    // 串口0中断函数
Input:          // 无
Return:         // 无
*******************************************************************************/
SI_INTERRUPT(UART0_ISR, UART0_IRQn)
{
  if (SCON0_TI == 1) // Check if transmit flag is set
  {
    tx_flag = 1;
    SCON0_TI = 0; // Clear interrupt flag
  }
  if (SCON0_RI == 1)
  {
    uart0.rx_buff[uart0.len++] = SBUF0;
    if (uart0.len == RX_BUF_MAX_LEN)
      uart0.len = 0;
    uart0.rx_buff[uart0.len]='\0';
    SCON0_RI = 0;
  }
}

